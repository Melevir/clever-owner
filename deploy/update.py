# -*- coding: utf-8 -*-
import subprocess

def update_commands():
    # Обновление окружения
    args = ['pip', 'install', '-r', 'requirements.txt']
    subprocess.call(args)

    # Запуск syncdb
    args = ['python', 'manage.py', 'syncdb', '--noinput']
    subprocess.call(args)

    # Запуск миграций
    args = ['python', 'manage.py', 'migrate', '--noinput']
    subprocess.call(args)

    # Повторный syncdb чтобы появились права доступа из моделей
    args = ['python', 'manage.py', 'syncdb', '--all', '--noinput']
    subprocess.call(args)

    # Создание рыбы
    args = ['python', 'manage.py', 'make_fish_data']
    subprocess.call(args)

    
if __name__ == '__main__':
    update_commands()
