#coding: utf-8
import os
import urllib2
import requests
from BeautifulSoup import BeautifulSoup

def strong_strip(str):
    return str.replace('\n', '').replace('\r', '').replace('\t', '')

def parse_vasko_mebel():
    MAX_ITEMS_AMOUNT = 50000
    headers = { 'User-Agent' : 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7',
                'Referer' : 'http://www.vasko-mebel.ru/'}
    urlopener = urllib2.build_opener(urllib2.HTTPCookieProcessor())
    for furniture_id in xrange(1, MAX_ITEMS_AMOUNT):
        url = 'http://www.vasko-mebel.ru/Go/ViewProduct/id=%s'%18823#furniture_id
        print url
        request = urllib2.Request(url, None, headers)
        try:
            html_page = urlopener.open(request).read().decode('cp1251')
        except urllib2.HTTPError, e:
            print 'error code: ', e.code
            continue
        soup_page = BeautifulSoup(html_page)
        name = soup_page.find('td', {'class': 'text'}).contents[0].renderContents()
        price = int(soup_page.find('form', {'name': 'form1'}).contents[1].renderContents()
                    .split(':')[1].split('&')[0].replace(' ', ''))
        supplier = soup_page.find(text=u'Бренд:').parent.parent.contents[8].renderContents()
        furniture_type = strong_strip(soup_page.find('span', {'class': 'path'}).contents[5])
        print furniture_type
        raw_input()


    
if __name__ == '__main__':
    parse_vasko_mebel()
