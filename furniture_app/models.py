#coding: utf-8
from django.db import models


class MultiColoredMixin(models.Model):
    class Meta:
        abstract = True
        
    colors = models.ManyToManyField('Color', verbose_name=u'Расцветка', blank=True)    


class VisibleItemsManager(models.Manager):
    def get_query_set(self):
        return super(VisibleItemsManager, self).get_query_set().filter(is_visible=True)


class VisibleMixin(models.Model):
    class Meta:
        abstract = True

    is_visible = models.BooleanField(u'Виден на сайте', default=False)
    objects = models.Manager()
    visible = VisibleItemsManager()

class FurnitureType(models.Model):
    class Meta:
        verbose_name = u"Тип мебели"
        verbose_name_plural = u"Типы мебели"

    name = models.CharField(u'Название',max_length=250,
                            help_text=u'Название типа мебели')

    def __unicode__(self):
        return self.name


class Furniture(MultiColoredMixin, VisibleMixin):
    class Meta:
        verbose_name = u"Мебель"
        verbose_name_plural = u"Мебель"

    name = models.CharField(u'Название', max_length=250, help_text=u'Название данной модели')
    furniture_type = models.ForeignKey('FurnitureType', verbose_name=u'Тип мебели', null=True)
    height = models.DecimalField(u'Высота', help_text=u'Высота в см', max_digits=8, decimal_places=2)
    width = models.DecimalField(u'Ширина', help_text=u'Ширина в см', max_digits=8, decimal_places=2)
    depth = models.DecimalField(u'Глубина', help_text=u'Глубинв в см', max_digits=8, decimal_places=2)
    supplier = models.ForeignKey('Supplier', verbose_name=u'Поставщик')
    description = models.TextField(u'Описание', blank=True)
    supplier_price = models.DecimalField(u'Цена закупки', max_digits=8, decimal_places=2)
    margin = models.DecimalField(u'Наценка', max_digits=8, decimal_places=2)
    main_image = models.ImageField(u'Основное изображение мебели', upload_to='furniture/')

    def __unicode__(self):
        return u'{0} ({1})'.format(self.name, self.supplier)

    def get_price_for_sale(self):
        return self.supplier_price + self.margin


class Supplier(models.Model):
    class Meta:
        verbose_name = u"Поставщик"
        verbose_name_plural = u"Поставщики"

    name = models.CharField(u'Название',max_length=250)
    contact_person_name = models.CharField(u'Имя контактного лица',max_length=250, blank=True)    
    contact_person_email = models.CharField(u'email контактного лица',max_length=250, blank=True)
    contact_person_phone = models.CharField(u'Телефон контактного лица',max_length=250, blank=True)        
    description = models.TextField(u'Дополнительная информация', blank=True)
    
    def __unicode__(self):
        return self.name


class Color(models.Model):
    class Meta:
        verbose_name = u"Цвет"
        verbose_name_plural = u"Цвета"

    name = models.CharField(u'Название',max_length=250)
    red = models.IntegerField(u'Красный', blank=True)
    green = models.IntegerField(u'Зеленый', blank=True)
    blue = models.IntegerField(u'Синий', blank=True)    

    def __unicode__(self):
        return u'{0}'.format(self.name)
