#coding: utf-8
import factory
import random

from furniture_app.models import Furniture, Color, Supplier, FurnitureType


class ColorFactory(factory.Factory):
    FACTORY_FOR = Color

    name = factory.Sequence(lambda a: random.choice(
        (u'красный', u'синий', u'зеленый')))
    red = factory.Sequence(lambda a: a)
    green = factory.Sequence(lambda a: 2 * a)
    blue = factory.Sequence(lambda a: 3 * a)


class SupplierFactory(factory.Factory):
    FACTORY_FOR = Supplier

    name = factory.Sequence(lambda a: 
        (u'ООО "Рога и копыта"',
         u'ООО "Мебель на вес"',
         u'ООО "Секондхенд мебель"')[int(a)])
    contact_person_name = u'Анатолий Сергеевич'
    contact_person_email = u'anatoliy_sergeevich@mail.ru'
    contact_person_phone = u'8 965 123 45 67'
    description = u'осел'


class FurnitureTypeFactory(factory.Factory):
    FACTORY_FOR = FurnitureType

    name = factory.Sequence(lambda a: random.choice((u'стол', u'полка', u'кровать')))
    

class FurnitureFactory(factory.Factory):
    FACTORY_FOR = Furniture

    name = factory.Sequence(lambda a:
                            (u'Прекрасный', u'Великолепный', u'Блестящий', u'Отличный', u'Гениальный')[int(a)])
    is_visible = True
    height = 380
    width = 80
    depth = 65
    description = u'Крутой шкаф. Или не шкаф.'
    supplier_price = 7200
    margin = 500
    supplier = factory.SubFactory(SupplierFactory)
    main_image = '/media/furniture/invaders.jpg'

    @factory.post_generation()
    def set_colors(self, create, extracted, **kwargs):
        [self.colors.add(ColorFactory()) for _ in xrange(3)]

    @factory.post_generation()
    def set_furniture_type(self, create, extracted, **kwargs):
        new_type = FurnitureTypeFactory.build()
        old_types = FurnitureType.objects.filter(name=new_type.name)
        if old_types:
            self.furniture_type = old_types[0]
        else:
            new_type.save()
            self.furniture_type = new_type            
        if create:
            self.save()
