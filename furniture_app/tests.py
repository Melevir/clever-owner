#coding: utf-8
from django.test import TestCase
from django.core.urlresolvers import reverse
from operator import attrgetter

from furniture_app.factories.furniture import FurnitureFactory
from furniture_app.models import Furniture

class CatalogTest(TestCase):
    def setUp(self):
        self.furniture = [FurnitureFactory()]
        
    def test_all_furniture_shows_up_on_index(self):
        response = self.client.get(reverse('furniture_list'))
        furniture_pks = [f.pk for f in response.context['furniture_list']]
        self.assertQuerysetEqual(Furniture.objects.all(), furniture_pks, transform=attrgetter('pk'))
        [self.assertContains(response, f.name) for f in Furniture.objects.all()]
