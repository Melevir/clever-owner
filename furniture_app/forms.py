#coding: utf-8
from django import forms

from models import Supplier, FurnitureType


class FurnitureSearchForm(forms.Form):
    name = forms.CharField(label=u'Название', required=False)
    furniture_type = forms.ModelChoiceField(label=u'Тип', queryset=FurnitureType.objects.all(), required=False)
    supplier = forms.ModelChoiceField(label=u'Производитель', queryset=Supplier.objects.all(), required=False)
