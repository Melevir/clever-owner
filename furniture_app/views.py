#coding: utf-8
from django.views.generic.list import ListView
from django.views.generic.edit import FormMixin
from django.views.generic import DetailView
from django.shortcuts import get_object_or_404, render, redirect
from django.http import Http404

from models import Furniture
from site_pages_app.models import EditableContentBlock
from forms import FurnitureSearchForm


class FurnitureListView(ListView):
    model = Furniture
    queryset = Furniture.visible.all()
    
    def get_context_data(self, **kwargs):
        context = super(FurnitureListView, self).get_context_data(**kwargs)
        context['form'] = kwargs.get('form', None) or FurnitureSearchForm()
        
        return context

    #TODO отрефакторить в соответствии с cbv
    def post(self, request, *args, **kwargs):
        form = FurnitureSearchForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            supplier_pk = form.cleaned_data['supplier']
            type_pk = form.cleaned_data['furniture_type']
        filtered_furniture = Furniture.objects.filter(name__icontains = name)
        if supplier_pk:
            filtered_furniture = filtered_furniture.filter(supplier = supplier_pk)
        if type_pk:
            filtered_furniture = filtered_furniture.filter(furniture_type = type_pk)
        self.object_list = self.get_queryset()            
        context = self.get_context_data(object_list=self.object_list, form=form)
        context['furniture_list'] = filtered_furniture
        return render(request, 'furniture_app/furniture_list.html', context)

class FurnitureDetailView(DetailView):
    model = Furniture

    def get_context_data(self, **kwargs):
        context = super(FurnitureDetailView, self).get_context_data(**kwargs)
        furniture = context['furniture']
        context['to_buy_block'] = get_object_or_404(EditableContentBlock, slug='to_buy')
        if not furniture.is_visible:
            raise Http404('No furniture found')
        else:
            return context
