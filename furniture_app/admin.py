from django.contrib import admin
from furniture_app import models

admin.site.register(models.Furniture)
admin.site.register(models.Supplier)
admin.site.register(models.Color)
