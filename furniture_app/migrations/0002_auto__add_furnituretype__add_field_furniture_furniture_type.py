# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'FurnitureType'
        db.create_table('furniture_app_furnituretype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=250)),
        ))
        db.send_create_signal('furniture_app', ['FurnitureType'])

        # Adding field 'Furniture.furniture_type'
        db.add_column('furniture_app_furniture', 'furniture_type',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['furniture_app.FurnitureType'], null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'FurnitureType'
        db.delete_table('furniture_app_furnituretype')

        # Deleting field 'Furniture.furniture_type'
        db.delete_column('furniture_app_furniture', 'furniture_type_id')


    models = {
        'furniture_app.color': {
            'Meta': {'object_name': 'Color'},
            'blue': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'green': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'red': ('django.db.models.fields.IntegerField', [], {'blank': 'True'})
        },
        'furniture_app.furniture': {
            'Meta': {'object_name': 'Furniture'},
            'colors': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['furniture_app.Color']", 'symmetrical': 'False', 'blank': 'True'}),
            'depth': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '2'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'furniture_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['furniture_app.FurnitureType']", 'null': 'True'}),
            'height': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '2'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'margin': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'supplier': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['furniture_app.Supplier']"}),
            'supplier_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '2'}),
            'width': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '2'})
        },
        'furniture_app.furnituretype': {
            'Meta': {'object_name': 'FurnitureType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '250'})
        },
        'furniture_app.supplier': {
            'Meta': {'object_name': 'Supplier'},
            'contact_person_email': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'contact_person_name': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'contact_person_phone': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        }
    }

    complete_apps = ['furniture_app']