# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Furniture'
        db.create_table('furniture_app_furniture', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('height', self.gf('django.db.models.fields.DecimalField')(max_digits=8, decimal_places=2)),
            ('width', self.gf('django.db.models.fields.DecimalField')(max_digits=8, decimal_places=2)),
            ('depth', self.gf('django.db.models.fields.DecimalField')(max_digits=8, decimal_places=2)),
            ('supplier', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['furniture_app.Supplier'])),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('supplier_price', self.gf('django.db.models.fields.DecimalField')(max_digits=8, decimal_places=2)),
            ('margin', self.gf('django.db.models.fields.DecimalField')(max_digits=8, decimal_places=2)),
        ))
        db.send_create_signal('furniture_app', ['Furniture'])

        # Adding M2M table for field colors on 'Furniture'
        db.create_table('furniture_app_furniture_colors', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('furniture', models.ForeignKey(orm['furniture_app.furniture'], null=False)),
            ('color', models.ForeignKey(orm['furniture_app.color'], null=False))
        ))
        db.create_unique('furniture_app_furniture_colors', ['furniture_id', 'color_id'])

        # Adding model 'Supplier'
        db.create_table('furniture_app_supplier', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('contact_person_name', self.gf('django.db.models.fields.CharField')(max_length=250, blank=True)),
            ('contact_person_email', self.gf('django.db.models.fields.CharField')(max_length=250, blank=True)),
            ('contact_person_phone', self.gf('django.db.models.fields.CharField')(max_length=250, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('furniture_app', ['Supplier'])

        # Adding model 'Color'
        db.create_table('furniture_app_color', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('red', self.gf('django.db.models.fields.IntegerField')(blank=True)),
            ('green', self.gf('django.db.models.fields.IntegerField')(blank=True)),
            ('blue', self.gf('django.db.models.fields.IntegerField')(blank=True)),
        ))
        db.send_create_signal('furniture_app', ['Color'])


    def backwards(self, orm):
        # Deleting model 'Furniture'
        db.delete_table('furniture_app_furniture')

        # Removing M2M table for field colors on 'Furniture'
        db.delete_table('furniture_app_furniture_colors')

        # Deleting model 'Supplier'
        db.delete_table('furniture_app_supplier')

        # Deleting model 'Color'
        db.delete_table('furniture_app_color')


    models = {
        'furniture_app.color': {
            'Meta': {'object_name': 'Color'},
            'blue': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'green': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'red': ('django.db.models.fields.IntegerField', [], {'blank': 'True'})
        },
        'furniture_app.furniture': {
            'Meta': {'object_name': 'Furniture'},
            'colors': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['furniture_app.Color']", 'symmetrical': 'False', 'blank': 'True'}),
            'depth': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '2'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'height': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '2'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'margin': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'supplier': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['furniture_app.Supplier']"}),
            'supplier_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '2'}),
            'width': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '2'})
        },
        'furniture_app.supplier': {
            'Meta': {'object_name': 'Supplier'},
            'contact_person_email': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'contact_person_name': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'contact_person_phone': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        }
    }

    complete_apps = ['furniture_app']