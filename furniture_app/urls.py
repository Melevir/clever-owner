from django.conf.urls import patterns, include, url
from django.contrib import admin

from views import FurnitureListView, FurnitureDetailView


urlpatterns = patterns('',
    url(r'^$', FurnitureListView.as_view(), name='furniture_list'),
    url(r'^(?P<pk>\d+)/$', FurnitureDetailView.as_view(), name='furniture_detail'),
)
