#coding: utf-8
import factory
import random

from site_pages_app.models import Page, EditableContentBlock

PAGES = (
    {'title': 'Главная', 'url': 'index', 'slug': 'index',
     'content': 'Добро пожаловать на сайт магазина "Хороший хозяин"!'},
    {'title': 'Контакты', 'url': 'contacts', 'slug': 'contacts',
     'content': '<b>Телефон: </b> 123-45-66'},
    {'title': 'Доставка', 'url': 'delivery', 'slug': 'delivery',
     'content': 'Доставка осуществляется по Ногинскому району. стоимость доставки - 200 руб.'}
    )

CONTENT_BLOCKS = (
    {'title': 'Блок информации при покупке товара', 'slug': 'to_buy',
     'content': 'Чтобы сделать заказ позвоните по телефону 123 45 67.'},
    )

class PageFactory(factory.Factory):
    FACTORY_FOR = Page
    
    is_visible = True
    title = factory.Sequence(lambda a: random.choice(PAGES)['title'])
    slug = factory.Sequence(lambda a: random.choice(PAGES)['slug'])
    url = factory.Sequence(lambda a: random.choice(PAGES)['url'])
    content = factory.Sequence(lambda a: random.choice(PAGES)['content'])

class EditableContentBlockFactory(factory.Factory):
    FACTORY_FOR = EditableContentBlock

    title = factory.Sequence(lambda a: random.choice(CONTENT_BLOCKS)['title'])
    slug = factory.Sequence(lambda a: random.choice(CONTENT_BLOCKS)['slug'])
    content = factory.Sequence(lambda a: random.choice(CONTENT_BLOCKS)['content'])
