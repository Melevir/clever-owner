# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'EditableContentBlock.is_visible'
        db.delete_column('site_pages_app_editablecontentblock', 'is_visible')

        # Adding unique constraint on 'EditableContentBlock', fields ['slug']
        db.create_unique('site_pages_app_editablecontentblock', ['slug'])


    def backwards(self, orm):
        # Removing unique constraint on 'EditableContentBlock', fields ['slug']
        db.delete_unique('site_pages_app_editablecontentblock', ['slug'])

        # Adding field 'EditableContentBlock.is_visible'
        db.add_column('site_pages_app_editablecontentblock', 'is_visible',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    models = {
        'site_pages_app.editablecontentblock': {
            'Meta': {'object_name': 'EditableContentBlock'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slug': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'site_pages_app.page': {
            'Meta': {'object_name': 'Page'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_visible': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['site_pages_app']