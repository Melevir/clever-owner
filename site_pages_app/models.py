#coding: utf-8
from django.db import models
from django.db.models import permalink

from furniture_app.models import VisibleMixin


class Page(VisibleMixin):
    class Meta:
        verbose_name = u"Страница сайта"
        verbose_name_plural = u"Страницы сайта"
    
    title = models.CharField(u'Название страницы', max_length=200)
    slug = models.CharField(u'slug', max_length=200, unique=True)
    url = models.CharField(u'URL', max_length=200)    
    content = models.TextField(u'Текст страницы')

    def __unicode__(self):
        return u'{0} ({1})'.format(self.title, self.url)

    @permalink
    def get_absolute_url(self):
        return 'page_detail', (self.url,)


class EditableContentBlock(models.Model):
    class Meta:
        verbose_name = u"Блок редактируемого контента"
        verbose_name_plural = u"Блоки редактируемого контента"
    
    title = models.CharField(u'Название блока', max_length=200)
    slug = models.CharField(u'slug', max_length=200, unique=True)
    content = models.TextField(u'Содержание блока')

    def __unicode__(self):
        return u'{0}'.format(self.title)
