from django.views.generic import DetailView

from models import Page

class PageDetailView(DetailView):
    model = Page
    slug_field = 'url'
    slug_url_kwarg = 'url'

        
