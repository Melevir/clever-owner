from django.conf.urls import patterns, include, url
from django.contrib import admin

from views import PageDetailView


urlpatterns = patterns('',
    url(r'^(?P<url>.*)/$', PageDetailView.as_view(), name='page_detail'),
)
