from django.contrib import admin
from site_pages_app import models

admin.site.register(models.Page)
admin.site.register(models.EditableContentBlock)
