#coding: utf-8
from django.views.generic.base import RedirectView
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse

from site_pages_app.models import Page

class IndexView(RedirectView):
    
    def get_redirect_url(self, **kwargs):
        #FIXME убрать захардкоженый урл
        index_page = get_object_or_404(Page, url='index')
        return reverse('page_detail', args=(index_page.url,))
