from django.conf.urls import patterns, include, url
from django.contrib import admin
from settings import MEDIA_ROOT

from views import IndexView

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', IndexView.as_view(), name='index'),

    # django apps
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),

    #files_backdoor
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
            {'document_root': MEDIA_ROOT, 'show_indexes': True }),

    # my apps
    url(r'^catalog/', include('furniture_app.urls')),

    # should be last
    url(r'^', include('site_pages_app.urls')),                       
)
