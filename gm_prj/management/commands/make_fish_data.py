#coding: utf-8
from django.core.management.base import BaseCommand, CommandError
import random

from site_pages_app.models import Page, EditableContentBlock
from furniture_app.models import Furniture, Color, Supplier, FurnitureType
from site_pages_app.factories.page import PageFactory, EditableContentBlockFactory, PAGES, CONTENT_BLOCKS
from furniture_app.factories.furniture import FurnitureFactory, SupplierFactory



class Command(BaseCommand):
    suppliers_amount = 3
    furniture_amount = 10

    def clear_db(self):
        Supplier.objects.all().delete()
        Color.objects.all().delete()
        Furniture.objects.all().delete()
        FurnitureType.objects.all().delete()
        Page.objects.all().delete()
        EditableContentBlock.objects.all().delete()
        
    def create_furniture(self):
        suppliers = []
        for _ in xrange(self.suppliers_amount):
            suppliers.append(SupplierFactory.create())

        for i in xrange(self.furniture_amount):
            FurnitureFactory.create(name=u'Название{0}'.format(i), supplier=random.choice(suppliers))

    def create_site_pages(self):
        for page in PAGES:
            PageFactory.create(title=page['title'], slug=page['slug'], url=page['url'], content=page['content'])

    def create_editable_content(self):
        for content in CONTENT_BLOCKS:
            EditableContentBlockFactory.create(title=content['title'], slug=content['slug'], content=content['content'])

    def handle(self, *args, **options):
        self.clear_db()
        self.create_furniture()
        self.create_site_pages()
        self.create_editable_content()
