#coding: utf-8
from django.test import TestCase
from django.core.urlresolvers import reverse


class AnonimousSmokeTest(TestCase):
    def test_catalog(self):
        response = self.client.get(reverse('furniture_list'))
        self.assertEqual(response.status_code, 200)
